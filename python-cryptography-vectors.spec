%global srcname cryptography-vectors
%global pysrcname cryptography_vectors

Name:           python-%{srcname}
Version:        3.2.1
Release:        2%{?dist}
License:        ASL 2.0 or BSD
Summary:        Test vectors for the cryptography package
URL:            https://pypi.python.org/pypi/cryptography-vectors
Source0:        https://pypi.io/packages/source/c/%{srcname}/cryptography_vectors-%{version}.tar.gz

BuildArch:      noarch

%description
Test vectors for the cryptography package.

The only purpose of this package is to be a building requirement for
python-cryptography, otherwise it has no use. Don’t install it unless
you really know what you are doing.

%package -n  python%{python3_pkgversion}-%{srcname}
Summary:        Test vectors for the cryptography package
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}

%description -n python%{python3_pkgversion}-%{srcname}
Test vectors for the cryptography package.

The only purpose of this package is to be a building requirement for
python-cryptography, otherwise it has no use. Don’t install it unless
you really know what you are doing.

%prep
%setup -q -n %{pysrcname}-%{version}
rm -vrf %{pysrcname}.egg-info

%build
%py3_build

%check
%{__python3} setup.py test

%install
%py3_install

%files -n python%{python3_pkgversion}-%{srcname}
%license LICENSE LICENSE.APACHE LICENSE.BSD
%{python3_sitelib}/%{pysrcname}/
%{python3_sitelib}/%{pysrcname}-%{version}-py*.egg-info

%changelog
* Wed Jan  6 14:37:40 CET 2021 Christian Heimes <cheimes@redhat.com> - 3.2.1-2
- Rebuild for gating

* Wed Oct 28 2020 Christian Heimes <cheimes@redhat.com> - 3.2.1-1
- Rebase to upstream release 3.2.1
- Resolves: rhbz#1873581

* Wed Jul 18 2018 Christian Heimes <cheimes@redhat.com> - 2.3-1
- New upstream release 2.3
- Fix AEAD tag truncation bug, RHBZ#1602755

* Tue Jun 19 2018 Christian Heimes <cheimes@redhat.com> - 2.2.1-2
- Drop Python 2 subpackages from RHEL 8, fixes RHBZ#1592761

* Wed Mar 21 2018 Christian Heimes <cheimes@redhat.com> - 2.2.1-1
- New upstream release 2.2.1

* Sun Feb 18 2018 Christian Heimes <cheimes@redhat.com> - 2.1.4-1
- New upstream release 2.1.4

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 2.1.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Nov 23 2017 Haïkel Guémar <hguemar@fedoraproject.org> - 2.1.3-1
- Upstream 2.1.3

* Tue Oct 17 2017 Igor Gnatenko <ignatenkobrain@fedoraproject.org> - 2.1-2
- Cleanups in packaging

* Thu Oct 12 2017 Christian Heimes <cheimes@redhat.com> - 2.1-1
- New upstream release 2.1

* Tue Sep 05 2017 Troy Dawson <tdawson@redhat.com> - 2.0.2-2
- Cleanup spec file conditionals

* Thu Aug 03 2017 Christian Heimes <cheimes@redhat.com> - 2.0.2-1
- New upstream release 2.0.2
- Modernize spec

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.9-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Wed Jul 05 2017 Randy Barlow <bowlofeggs@fedoraproject.org> - 1.9-2
- BuildRequire python2-setuptools instead of python-setuptools.
- Use the license macro.

* Tue Jun 27 2017 Christian Heimes <cheimes@redhat.com> - 1.9-1
- Upstream release 1.9

* Wed Feb 15 2017 Christian Heimes <cheimes@redhat.com> - 1.7.2-1
- Update to latest upstream

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.7.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Thu Jan 05 2017 Matěj Cepl <mcepl@redhat.com> - 1.7.1-1
- Update to v1.7.1

* Mon Dec 12 2016 Charalampos Stratakis <cstratak@redhat.com> - 1.5.3-2
- Rebuild for Python 3.6

* Mon Nov 07 2016 Nathaniel McCallum <npmccallum@redhat.com> - 1.5.3-1
- Update to v1.5.3
- Update source URL

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3.1-3
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Tue May 10 2016 Nathaniel McCallum <npmccallum@redhat.com> - 1.3.1-2
- Clean up distro macros

* Tue May 03 2016 Nathaniel McCallum <npmccallum@redhat.com> - 1.3.1-1
- Update to v1.3.1

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Mon Jan 11 2016 Nathaniel McCallum <npmccallum@redhat.com> - 1.2.1-2
- Migrate python-cryptography-vectors => python2-cryptography-vectors

* Sat Jan 09 2016 Nathaniel McCallum <npmccallum@redhat.com> - 1.2.1-1
- Update to v1.2.1

* Wed Nov 11 2015 Robert Kuska <rkuska@redhat.com> - 1.1-1
- Update to v1.1

* Wed Nov 04 2015 Robert Kuska <rkuska@redhat.com> - 1.0.2-2
- Rebuilt for Python3.5 rebuild

* Wed Sep 30 2015 Matěj Cepl <mcepl@redhat.com> - 1.0.2-1
- New upstream release (fix #1267548)

* Wed Aug 12 2015 Nathaniel McCallum <npmccallum@redhat.com> - 1.0-1
- New upstream release

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Thu May 14 2015 Nathaniel McCallum <npmccallum@redhat.com> - 0.9-1
- New upstream release

* Fri Apr 17 2015 Nathaniel McCallum <npmccallum@redhat.com> - 0.8.2-1
- New upstream release

* Fri Mar 13 2015 Nathaniel McCallum <npmccallum@redhat.com> - 0.8-1
- New upstream release

* Wed Mar 04 2015 Nathaniel McCallum <npmccallum@redhat.com> - 0.7.2-2
- Add python3 subpackage

* Wed Mar 04 2015 Nathaniel McCallum <npmccallum@redhat.com> - 0.7.2-1
- New upstream release
- Now licensed under Apache 2.0 or BSD

* Thu Oct 16 2014 Matej Cepl <mcepl@redhat.com> - 0.6.1-1
- New upstream release (fixes among others #1153501)

* Wed Oct 01 2014 Matej Cepl <mcepl@redhat.com> - 0.5.4-3
- Add LICENSE file from the upstream repo.

* Mon Sep 29 2014 Matej Cepl <mcepl@redhat.com> - 0.5.4-2
- initial package for Fedora
